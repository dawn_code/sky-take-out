package com.sky.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 地址位置的经纬度
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Location {
    private String lat; //纬度
    private String lng; //经度

    public String getLatLng(){
        return lat + "," + lng;
    }
}
