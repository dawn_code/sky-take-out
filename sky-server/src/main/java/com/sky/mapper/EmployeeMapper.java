package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工信息
     * @param username
     * @return
     */
    @Select("select id, name, username, password, phone, sex, id_number, status, create_time, update_time, create_user, update_user from employee where username = #{username}")
    public Employee findByUsername(String username);

    /**
     * 新增员工
     */
    @AutoFill(OperationType.INSERT)
    @Insert("insert into employee (name, username, password, phone, sex, id_number, status, create_time, update_time, create_user, update_user) " +
            "values (#{name}, #{username}, #{password}, #{phone},#{sex},#{idNumber},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void insert(Employee employee);

    /**
     * 动态条件查询
     * @param name
     * @return
     */
    List<Employee> list(String name);

    /**
     * 更新员工
     * @param employee
     */
    @AutoFill(OperationType.UPDATE)
    void update(Employee employee);

    //根据ID查询员工信息
    @Select("select * from employee where id = #{id}")
    Employee getById(Long id);
}
