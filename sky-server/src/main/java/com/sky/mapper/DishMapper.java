package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */
    @Select("select count(*) from dish where category_id = #{categoryId}")
    Long countByCategoryId(Long categoryId);

    /**
     * 保存菜品基本信息
     * @param dish
     */
    @AutoFill(OperationType.INSERT)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into dish (name, category_id, price, image, description, status, create_time, update_time, create_user, update_user) VALUES " +
            "(#{name}, #{categoryId}, #{price}, #{image}, #{description}, #{status}, #{createTime}, #{updateTime}, #{createUser}, #{updateUser})")
    void insert(Dish dish);

    /**
     * 条件查询
     * @param pageQueryDTO
     * @return
     */
    List<DishVO> list(DishPageQueryDTO pageQueryDTO);

    /**
     * 根据ID统计起售菜品的数量
     * @param ids
     * @return
     */
    Long countEnableDishByIds(List<Long> ids);

    /**
     * 批量删除菜品数据
     * @param ids
     */
    void deleteByIds(List<Long> ids);

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    @Select("select * from dish where id = #{id}")
    Dish getById(Long id);

    /**
     * 动态修改菜品信息
     * @param dish
     */
    @AutoFill(OperationType.UPDATE)
    void update(Dish dish);

    /**
     * 根据套餐ID查询
     * @param setmealId
     * @return
     */
    @Select("select d.* from dish d left join setmeal_dish sd on d.id = sd.dish_id where sd.setmeal_id = #{setmealId}")
    List<Dish> getBySetmealId(Long setmealId);

    /**
     * 根据分类ID, name查询菜品
     * @param categoryId
     * @param name
     * @return
     */
    List<Dish> selectDishByCondition(Long categoryId, String name);


    /**
     * 查询菜品及关联的数据
     * @param dish
     * @return
     */
    List<DishVO> listDishWithFlavors(Dish dish);

    /**
     * 统计启售和停售菜品数量
     */
    DishOverViewVO countByStatus();
}
