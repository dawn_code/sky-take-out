package com.sky.mapper;

import com.sky.dto.UserReportDTO;
import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * 根据openid获取用户信息
     * @param openid
     * @return
     */
    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where openid = #{openid}")
    User selectByOpenid(String openid);

    /**
     * 插入用户信息
     * @param user
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into user (openid, name, phone, sex, id_number, avatar, create_time) VALUES " +
            "(#{openid}, #{name}, #{phone}, #{sex}, #{idNumber}, #{avatar}, #{createTime} )")
    void insert(User user);


    @Select("select * from user where  id = #{id}")
    public User getById(Long id);

    /**
     * 统计每一天的新增用户数
     */
    List<UserReportDTO> countAddByCreateTime(LocalDateTime beginTime, LocalDateTime endTime);

    /**
     * 统计 截止到指定时间的 用户的总数量
     * @param beginTime
     * @return
     */
    @Select("select count(*) from user where create_time < #{beginTime} ")
    Integer countTotalByCreateTime(LocalDateTime beginTime);

    /**
     * 根据时间范围统计新增用户数
     */
    Integer countByTime(LocalDateTime begin, LocalDateTime end);
}
