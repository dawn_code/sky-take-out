package com.sky.mapper;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.entity.Orders;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrdersMapper {

    /**
     * 新增订单
     * @param orders
     */
    void insert(Orders orders);


    /**
     * 根据订单号和用户id查询订单
     */
    @Select("select * from orders where number = #{orderNumber}")
    Orders getByNumber(String orderNumber);


    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);



    /**
     * 根据条件查询订单信息
     * @param ordersPageQueryDTO
     * @return
     */
    List<Orders> list(OrdersPageQueryDTO ordersPageQueryDTO);

    /**
     * 根据订单id查询订单信息
     * @param id
     * @return
     */
    @Select("select * from orders where  id = #{id}")
    Orders getById(Long id);

    /**
     * 统计指定状态的订单数量
     * @param status
     * @return
     */
    @Select("select count(*) from orders where status = #{status}")
    Integer countStatus(Integer status);


    /**
     * 查询指定状态 且 指定时间之前下单的订单
     * @return
     */
    @Select("select * from orders where status = #{status} and order_time < #{beforeTime}")
    List<Orders> selectByStatusAndLtTime(Integer status, LocalDateTime beforeTime);

    /**
     * 统计指定状态, 指定时间段的应用额数据
     */
    List<TurnoverReportDTO> selectTurnoverList(LocalDateTime beginTime, LocalDateTime endTime, Integer status);

    /**
     * 根据指定时间段, 查询指定状态的订单数量
     */
    List<OrderReportDTO> countOrderByOrderTimeAndStatus(LocalDateTime beginTime, LocalDateTime endTime, Integer status);

    /**
     * 统计指定时间区间内 , 菜品销量的Top10
     */
    List<SalesReportDTO> getSalesTop10(LocalDateTime beginTime, LocalDateTime endTime);


    /**
     * 统计订单信息
     */
    BusinessDataVO countByTime(LocalDateTime begin, LocalDateTime end);


    /**
     * 统计各种状态的订单数量
     */
    OrderOverViewVO countOrderByTime(LocalDateTime begin, LocalDateTime end);

}
