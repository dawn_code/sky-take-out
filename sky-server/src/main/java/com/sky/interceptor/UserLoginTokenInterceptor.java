package com.sky.interceptor;

import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 校验令牌的拦截器-小程序端
 */
@Slf4j
@Component
public class UserLoginTokenInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtProperties jwtProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("拦截到了请求 , {}" , request.getRequestURL().toString());

        //1. 获取请求头中的令牌
        String jwt = request.getHeader(jwtProperties.getUserTokenName());

        //2. 判断令牌是否存在, 如果不存在, 则不放行 - 401
        if(!StringUtils.hasLength(jwt)){
            log.info("令牌为空, 响应401");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }

        //3. 校验令牌 , 如果令牌校验失败 , 则不放行 - 401
        try {
            Claims claims = JwtUtil.parseJWT(jwtProperties.getUserSecretKey(), jwt);
            log.info("解析完令牌, {}", claims);
            Long userId = Long.valueOf(claims.get(JwtClaimsConstant.USER_ID).toString());

            //存入当前用户ID
            BaseContext.setCurrentId(userId);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("令牌非法, 响应401");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }

        //4. 放行
        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        BaseContext.removeCurrentId();
    }
}
