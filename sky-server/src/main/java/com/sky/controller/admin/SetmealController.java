package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/setmeal")
@Api(tags = "套餐管理")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    /**
     * 新增套餐
     * @param setmealDTO
     * @return
     */
    @PostMapping
    @ApiOperation("新增套餐")
    public Result save(@RequestBody SetmealDTO setmealDTO){
        log.info("新增套餐信息, {}", setmealDTO);
        setmealService.save(setmealDTO);
        return Result.success();
    }

    /**
     * 套餐分页查询
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("套餐分页查询")
    public Result<PageResult> page(SetmealPageQueryDTO setmealPageQueryDTO){
        log.info("套餐数据分页查询,{}");
        PageResult pageResult = setmealService.page(setmealPageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 批量删除套餐
     * @return
     */
    @DeleteMapping
    @ApiOperation("批量删除套餐")
    public Result delete(@RequestParam List<Long> ids){
        log.info("批量删除套餐 , {}", ids);
        setmealService.delete(ids);
        return Result.success();
    }

    /**
     * 根据ID查询套餐数据, 包括套餐关联的菜品列表
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation("根据ID查询套餐")
    public Result<SetmealVO> getById(@PathVariable Long id){
        log.info("根据ID查询套餐数据 , id : {}" , id);
        SetmealVO setmealVO = setmealService.getById(id);
        return Result.success(setmealVO);
    }


    /**
     * 更新套餐数据
     * @return
     */
    @PutMapping
    @ApiOperation("更新套餐信息")
    public Result update(@RequestBody SetmealDTO setmealDTO){
        log.info("更新套餐数据, {}" , setmealDTO);
        setmealService.update(setmealDTO);
        return Result.success();
    }


    /**
     * 起售/停售套餐
     * @param status
     * @param id
     * @return
     */
    @PutMapping("/status/{status}/{id}")
    public Result startOrStop(@PathVariable Integer status , @PathVariable Long id){
        log.info("起售/停售套餐 , id:{} , status:{}", id, status);
        setmealService.startOrStop(status, id);
        return Result.success();
    }
}
