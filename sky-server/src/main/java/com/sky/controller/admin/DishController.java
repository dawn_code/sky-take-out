package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "菜品管理")
@RestController
@RequestMapping("/admin/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    /**
     * 新增菜品
     * @return
     */
    @ApiOperation("新增菜品")
    @PostMapping
    public Result save(@RequestBody DishDTO dishDTO){
        log.info("新增菜品数据, {}", dishDTO);
        dishService.save(dishDTO);
        return Result.success();
    }

    /**
     * 分页条件查询
     * @return
     */
    @ApiOperation("分页条件查询")
    @GetMapping("/page")
    public Result<PageResult> page(DishPageQueryDTO pageQueryDTO){
        log.info("分页条件查询 , {}", pageQueryDTO);
        PageResult pageResult = dishService.page(pageQueryDTO);
        return Result.success(pageResult);
    }


    /**
     * 批量删除菜品
     * @return
     */
    @ApiOperation("批量删除菜品")
    @DeleteMapping
    public Result delete(@RequestParam List<Long> ids){
        log.info("批量删除菜品 {}", ids);
        dishService.delete(ids);
        return Result.success();
    }

    /**
     * 根据ID查询用于页面回显
     * @return
     */
    @ApiOperation("根据ID查询")
    @GetMapping("/{id}")
    public Result<DishVO> getInfo(@PathVariable Long id) {
        log.info("根据ID查询用于页面回显 , {}" , id);
        DishVO dishVO = dishService.getInfo(id);
        return Result.success(dishVO);
    }


    /**
     * 修改
     * @return
     */
    @ApiOperation("修改")
    @PutMapping
    public Result update(@RequestBody DishDTO dishDTO){
        log.info("修改菜品, {}", dishDTO);
        dishService.update(dishDTO);
        return Result.success();
    }


    /**
     * 起售/停售菜品
     * @return
     */
    @PutMapping("/status/{status}/{id}")
    @ApiOperation("起售/停售菜品")
    public Result startOrStop(@PathVariable Integer status , @PathVariable Long id){
        log.info("起售/停售菜品信息, status:{}, id:{}", status, id);
        dishService.startOrStop(id , status);
        return Result.success();
    }


    /**
     * 根据分类ID查询菜品数据
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("根据分类ID查询菜品数据")
    public Result<List<Dish>> list(Long categoryId, String name){
        log.info("查询分类id为 {} 的菜品数据", categoryId);
        List<Dish> dishList = dishService.list(categoryId, name);
        return Result.success(dishList);
    }
}
