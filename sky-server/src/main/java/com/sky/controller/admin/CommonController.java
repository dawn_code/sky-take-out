package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@Slf4j
@Api(tags = "通用接口")
@RestController
@RequestMapping("/admin/common")
public class CommonController {

    @Autowired
    private AliOssUtil aliOssUtil;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file) throws IOException {
        log.info("文件上传 , {}" , file);
        //1. 获取文件的拓展名
        String originalFilename = file.getOriginalFilename(); //1.1.11.jpg
        String extName = originalFilename.substring(originalFilename.lastIndexOf("."));

        //2. 构建一个新的文件名
        String objectName = UUID.randomUUID().toString() + extName;

        //3. 调用工具类上传文件
        String url = aliOssUtil.upload(file.getBytes(), objectName);
        return Result.success(url);
    }

}
