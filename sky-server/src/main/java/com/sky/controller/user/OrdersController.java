package com.sky.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.sky.dto.OrdersPaymentDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrdersService;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.websocket.WebSocketServer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Api(tags = "C端-订单操作")
@RestController
@RequestMapping("/user/order")
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private WebSocketServer webSocketServer;

    @ApiOperation("提交订单")
    @PostMapping("/submit")
    public Result<OrderSubmitVO> submit(@RequestBody OrdersSubmitDTO ordersSubmitDTO) throws Exception {
        log.info("用户下单 ...");
        OrderSubmitVO orderSubmitVO = ordersService.submit(ordersSubmitDTO);
        return Result.success(orderSubmitVO);
    }


    /**
     * 订单支付
     *
     * @param ordersPaymentDTO
     * @return
     */
    @PutMapping("/payment")
    @ApiOperation("订单支付")
    public Result<OrderPaymentVO> payment(@RequestBody OrdersPaymentDTO ordersPaymentDTO) throws Exception {
        log.info("订单支付：{}", ordersPaymentDTO);
        OrderPaymentVO orderPaymentVO = ordersService.payment(ordersPaymentDTO);
        log.info("生成预支付交易单：{}", orderPaymentVO);
        return Result.success(orderPaymentVO);
    }



    /**
     * 历史订单查询接口
     * @param page
     * @param pageSize
     * @param status
     * @return
     */
    @GetMapping("/historyOrders")
    @ApiOperation("历史订单查询")
    public Result<PageResult> page(Integer page , Integer pageSize , Integer status){
        log.info("查询历史订单信息 , page:{}, pageSize:{}, status:{}" , page, pageSize, status);
        PageResult pageResult = ordersService.pageOrder4User(page, pageSize, status);
        return Result.success(pageResult);
    }


    /**
     * 根据ID查询订单详情信息
     * @param id
     * @return
     */
    @GetMapping("/orderDetail/{id}")
    @ApiOperation("根据ID查询订单详情信息")
    public Result<OrderVO> getDetailsById(@PathVariable Long id){
        log.info("根据ID查询订单详情信息 , id:{}" , id);
        OrderVO orderVO = ordersService.getDetailById(id);
        return Result.success(orderVO);
    }


    /**
     * 取消订单接口
     * @return
     */
    @PutMapping("/cancel/{id}")
    @ApiOperation("取消订单")
    public Result cancelOrder(@PathVariable Long id) throws Exception {
        log.info("取消订单, 订单ID: {}" , id);
        ordersService.cancelOrder(id);
        return Result.success();
    }


    /**
     * 再来一单
     * @param id
     * @return
     */
    @PostMapping("/repetition/{id}")
    @ApiOperation("再来一单")
    public Result repetition(@PathVariable Long id){
        log.info("再来一单 , 订单ID: {}" , id);
        ordersService.repetition(id);
        return Result.success();
    }


    /**
     * 客户催单
     * @return
     */
    @ApiOperation("客户催单")
    @GetMapping("/reminder/{id}")
    public Result reminder(@PathVariable Long id) throws Exception {
        log.info("客户催单, 订单ID: {}", id);

        OrderVO orderVO = ordersService.getDetailById(id);

        //基于Websocket给客户端推送消息 --- {"type": 2, "orderId": 12, "content": "订单号: 273872873823"}
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("type", 2);
        paramMap.put("orderId", id);
        paramMap.put("content", "订单号: "+orderVO.getNumber());
        log.info("向客户端推送消息 , {}", paramMap);

        webSocketServer.sendMessageToAllClient(JSONObject.toJSONString(paramMap));

        return Result.success();
    }
}
