package com.sky.aspect;

import com.sky.annotation.AutoFill;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Slf4j
@Aspect //切面类
@Component
public class AutoFillAspect {

    /**
     * 为公共字段进行属性填充
     */
    /*@Before("execution(* com.sky.mapper.*.*(..)) && @annotation(com.sky.annotation.AutoFill)")
    public void autoFillProperty(JoinPoint joinPoint) throws Exception {
        log.info("进入AOP程序, 为公共属性开始赋值 ");
        //1. 获取原始方法运行时传入的参数 , 获取第一个 (对象)
        Object[] args = joinPoint.getArgs();
        if(ObjectUtils.isEmpty(args)){ //参数为空
            return;
        }
        Object obj = args[0];

        log.info("为公共属性开始赋值, 赋值前: {} ", obj);

        //2. 通过反射获取到对象对应方法 [ 4个 ]公共属性
        Method setCreateTime = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
        Method setUpdateTime = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
        Method setCreateUser = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class);
        Method setUpdateUser = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);

        //3. 获取注解对应的value属性
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        AutoFill autoFill = methodSignature.getMethod().getAnnotation(AutoFill.class);//获取到原始方法上加的AutoFill注解
        OperationType operationType = autoFill.value();

        //4. 判断, 如果是insert , 为4个属性复制
        if(operationType.equals(OperationType.INSERT)){
            setCreateTime.invoke(obj, LocalDateTime.now());
            setCreateUser.invoke(obj, BaseContext.getCurrentId());
        }

        setUpdateTime.invoke(obj, LocalDateTime.now());
        setUpdateUser.invoke(obj, BaseContext.getCurrentId());

        log.info("为公共属性开始赋值, 赋值后: {} ", obj);
    }*/



    @Before("execution(* com.sky.mapper.*.*(..)) && @annotation(autoFill)")
    public void autoFillProperty(JoinPoint joinPoint , AutoFill autoFill) throws Exception {
        log.info("进入AOP程序, 为公共属性开始赋值 ");
        //1. 获取原始方法运行时传入的参数 , 获取第一个 (对象)
        Object[] args = joinPoint.getArgs();
        if(ObjectUtils.isEmpty(args)){ //参数为空
            return;
        }
        Object obj = args[0];

        log.info("为公共属性开始赋值, 赋值前: {} ", obj);

        //2. 通过反射获取到对象对应方法 [ 4个 ]公共属性
        Method setCreateTime = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
        Method setUpdateTime = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
        Method setCreateUser = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, Long.class);
        Method setUpdateUser = obj.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, Long.class);

        //3. 获取注解对应的value属性
        OperationType operationType = autoFill.value();

        //4. 判断, 如果是insert , 为4个属性复制
        if(operationType.equals(OperationType.INSERT)){
            setCreateTime.invoke(obj, LocalDateTime.now());
            setCreateUser.invoke(obj, BaseContext.getCurrentId());
        }

        setUpdateTime.invoke(obj, LocalDateTime.now());
        setUpdateUser.invoke(obj, BaseContext.getCurrentId());

        log.info("为公共属性开始赋值, 赋值后: {} ", obj);
    }


}
