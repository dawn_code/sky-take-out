package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {

    /**
     * 新增菜品
     * @param dishDTO
     */
    void save(DishDTO dishDTO);

    /**
     * 分页查询
     * @param pageQueryDTO
     * @return
     */
    PageResult page(DishPageQueryDTO pageQueryDTO);

    /**
     * 批量删除菜品
     * @param ids
     */
    void delete(List<Long> ids);

    /**
     * 根据ID查询菜品
     * @param id
     * @return
     */
    DishVO getInfo(Long id);

    /**
     * 修改
     * @param dishDTO
     */
    void update(DishDTO dishDTO);

    /**
     * 起售/停售菜品数据
     * @param id
     * @param status
     */
    void startOrStop(Long id, Integer status);

    /**
     * 根据分类ID查询菜品数据
     * @param categoryId
     * @return
     */
    List<Dish> list(Long categoryId, String name);

    /**
     * 查询菜品, 及菜品口味数据
     * @param categoryId
     * @return
     */
    List<DishVO> listDishWithFlavors(Long categoryId);
}
