package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.exception.DataException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.BeanHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final String LOGIN_PASSWORD_ERROR_KEY = "login:error:"; //密码错误标记的key
    private static final String LOGIN_LOCK_ERROR_KEY = "login:lock:"; //账号被锁定的key

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;


    @Override
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        // 校验员工账号是否被锁定 .
        validateAccountLock(username);

        //1. 调用Mapper, 查询这个员工信息
        Employee employee = employeeMapper.findByUsername(employeeLoginDTO.getUsername());

        //2. 判断员工是否存在, 不存在 , 返回错误信息
        if (employee == null) {
            log.info("查询到的员工信息为空 , 返回错误信息 ");
            throw new DataException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //3. 校验密码的正确性, 如果密码不正确 , 返回错误信息
        password = DigestUtils.md5DigestAsHex(password.getBytes());//对页面传递过来的明文进行加密
        if (!password.equals(employee.getPassword())) {
            log.info("密码比对错误");

            //3.1 记录员工密码错误的标记 , 并设置有效期为5分钟 .
            redisTemplate.opsForValue().set(getKey(username), "-", 5, TimeUnit.MINUTES);

            //3.2 获取该员工的密码错误标记 , 如果标记的数量 >= 5 , 设置账号锁定的标记
            Set<Object> keys = redisTemplate.keys(LOGIN_PASSWORD_ERROR_KEY + username + ":*");

            if (keys != null && keys.size() >= 5) {
                log.info("员工登录密码5分钟之内, 错误次数超过五次, 锁定账号一小时");
                redisTemplate.opsForValue().set(LOGIN_LOCK_ERROR_KEY + username, "-", 1, TimeUnit.HOURS);
                throw new BusinessException(MessageConstant.LOGIN_LOCK_ERROR);
            }

            throw new BusinessException(MessageConstant.PASSWORD_ERROR);
        }

        //4. 校验员工的状态 , 如果是禁用状态, 则返回错误信息
        if (employee.getStatus() == StatusConstant.DISABLE) {
            log.info("登录账号 {} 被禁用, 禁止登录 .", employeeLoginDTO.getUsername());
            throw new BusinessException(MessageConstant.ACCOUNT_LOCKED);
        }

        return employee;
    }

    private void validateAccountLock(String username) {
        Object flag = redisTemplate.opsForValue().get(LOGIN_LOCK_ERROR_KEY + username);
        if (ObjectUtils.isNotEmpty(flag)) { //账号被锁定
            log.info("账号被锁定, 限制登录");
            throw new BusinessException(MessageConstant.LOGIN_LOCK_ERROR_MESSAGE);
        }
    }


    //拼接redis的key
    private String getKey(String username) {
        return LOGIN_PASSWORD_ERROR_KEY + username + ":" + RandomStringUtils.randomAlphabetic(5);
    }






    @Override
    public void save(EmployeeDTO employeeDTO) {
        //1. 补全实体属性
        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);

        //设置密码-加密
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        employee.setStatus(StatusConstant.ENABLE); //启用

//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
//
//        employee.setCreateUser(BaseContext.getCurrentId()); //当前登录员工ID
//        employee.setUpdateUser(BaseContext.getCurrentId()); //当前登录员工ID

        //2. 调用mapper, 保存数据
        employeeMapper.insert(employee);
    }


    @Override
    public PageResult page(EmployeePageQueryDTO pageQueryDTO) {
        //1. 设置分页参数
        PageHelper.startPage(pageQueryDTO.getPage(), pageQueryDTO.getPageSize());

        //2. 执行查询
        List<Employee> employeeList = employeeMapper.list(pageQueryDTO.getName());

        //3. 解析并封装结果
        Page<Employee> page = (Page<Employee>) employeeList;
        return new PageResult(page.getTotal(), page.getResult());
    }


    @Override
    public void enableOrDisable(Integer status, Long id) {
        Employee employee = Employee.builder()
                .id(id)
                .status(status)
//                .updateTime(LocalDateTime.now())
//                .updateUser(BaseContext.getCurrentId())
                .build();
        employeeMapper.update(employee);
    }


    @Override
    public Employee getById(Long id) {
        return employeeMapper.getById(id);
    }

    @Override
    public void update(EmployeeDTO employeeDTO) {
        //实体属性拷贝
        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);

//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(BaseContext.getCurrentId());

        employeeMapper.update(employee);
    }
}
