package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import com.sky.utils.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    // 添加购物车
    @Override
    public void add(ShoppingCartDTO shoppingCartDTO) {
        //1. 查询 当前用户的购物车中是否存在指定的商品 (菜品-口味, 套餐) .
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());

        List<ShoppingCart> shoppingCartList =  shoppingCartMapper.list(shoppingCart);

        //2. 如果该商品在当前用户的购物车已经存在, 则数量+1 .
        if(!CollectionUtils.isEmpty(shoppingCartList)){
            ShoppingCart cart = shoppingCartList.get(0);
            cart.setNumber(cart.getNumber() + 1);
            shoppingCartMapper.updateNumberById(cart);
        }else {
            //3. 如果该商品在当前用户的购物车不存在, 则添加一条新的购物车数据 .
            Long dishId = shoppingCart.getDishId();
            if(dishId != null){ //菜品
                Dish dish = dishMapper.getById(dishId);
                shoppingCart.setAmount(dish.getPrice());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setName(dish.getName());
            } else  { //套餐
                Setmeal setmeal = setmealMapper.getById(shoppingCart.getSetmealId());
                shoppingCart.setAmount(setmeal.getPrice());
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setName(setmeal.getName());
            }

            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCart.setNumber(1);

            //插入数据
            shoppingCartMapper.insert(shoppingCart);
        }

    }


    @Override
    public List<ShoppingCart> list() {
        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();
        return shoppingCartMapper.list(shoppingCart);
    }


    @Override
    public void subShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        //1. 查询当前用户、当前操作的菜品/套餐的购物车列表数据
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.list(shoppingCart);

        //2. 判断当前购物车商品的数量 , 如果 > 1 , 就将数量减一, 然后更新数据库
        if(!CollectionUtils.isEmpty(shoppingCartList)){
            shoppingCart = shoppingCartList.get(0);
            Integer number = shoppingCart.getNumber();
            if(number > 1){
                shoppingCart.setNumber(shoppingCart.getNumber() - 1);
                shoppingCartMapper.updateNumberById(shoppingCart);
            } else {
                //3. 如果 = 1, 就删除该购物车商品
                shoppingCartMapper.deleteById(shoppingCart.getId());
            }
        }
    }


    @Override
    public void cleanShoppingCart() {
        shoppingCartMapper.deleteByUserId(BaseContext.getCurrentId());
    }
}
