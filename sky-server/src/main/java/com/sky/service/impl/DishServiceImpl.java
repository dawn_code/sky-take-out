package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Transactional
    @Override
    public void save(DishDTO dishDTO) {
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);

        //1. 保存菜品基本信息
        dish.setStatus(StatusConstant.DISABLE);
        dishMapper.insert(dish);

        //2. 保存菜品口味信息
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if (!CollectionUtils.isEmpty(flavors)) {
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dish.getId());
            });
            dishFlavorMapper.insertBatch(flavors);
        }

        //3. 删除redis缓存中的菜品数据
        cleanCache(dishDTO.getCategoryId().toString());
        log.info("新增菜品 , 清理缓存中对应的菜品数据");
    }


    @Override
    public PageResult page(DishPageQueryDTO pageQueryDTO) {
        //1. 设置分页参数
        PageHelper.startPage(pageQueryDTO.getPage(), pageQueryDTO.getPageSize());

        //2. 执行查询
        List<DishVO> dishVOList = dishMapper.list(pageQueryDTO);

        //3. 解析并封装分页结果
        Page<DishVO> page = (Page<DishVO>) dishVOList;
        return new PageResult(page.getTotal(), page.getResult());
    }


    @Transactional
    @Override
    public void delete(List<Long> ids) {
        //1. 判断菜品的状态, 起售中的菜品不能删除 , 提示错误信息
        Long count =  dishMapper.countEnableDishByIds(ids);
        if(count > 0){ //这批菜品中包含了起售菜品
            throw new BusinessException(MessageConstant.DISH_ON_SALE);
        }

        //2. 判断菜品是否关联套餐 , 如果关联套餐 , 不能删除 , 提示错误信息
        List<Long> setmealIds = setmealDishMapper.getSetmealIdsByDishIds(ids); //select setmeal_id from setmeal_dish where dish_id in (1,2,3);
        if(!CollectionUtils.isEmpty(setmealIds)){//关联了套餐
            throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

        //3. 删除菜品 , 并删除菜品口味
        dishMapper.deleteByIds(ids);
        dishFlavorMapper.deleteByDishIds(ids);


        //3. 删除redis缓存中的菜品数据
        cleanCache("*");
    }

    private void cleanCache(String suffix) {
        log.info("清理缓存中指定的key {}", "dish:cache:"+suffix);
        Set<Object> keys = redisTemplate.keys("dish:cache:"+suffix);
        redisTemplate.delete(keys);
    }


    @Override
    public DishVO getInfo(Long id) {
        //1. 根据ID查询菜品的基本信息  - Dish
        Dish dish = dishMapper.getById(id);

        //2. 根据ID查询菜品口味列表信息 - List<DishFlavor>
        List<DishFlavor> flavorList = dishFlavorMapper.getByDishId(id);

        //3. 组装数据
        DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);
        if(dishVO != null) {
            dishVO.setFlavors(flavorList);
        }
        return dishVO;
    }


    @Transactional
    @Override
    public void update(DishDTO dishDTO) {
        //1. 根据ID修改菜品基本信息 -- dish
        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        dishMapper.update(dish);

        //2. 根据菜品ID修改菜品口味信息 -- dish_flavor (先 删 , 后 加)
        //2.1 根据菜品ID删除口味数据
        dishFlavorMapper.deleteByDishIds(Collections.singletonList(dish.getId()));

        //2.2 再添加
        List<DishFlavor> flavors = dishDTO.getFlavors();
        if(!CollectionUtils.isEmpty(flavors)){
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dish.getId());
            });
            dishFlavorMapper.insertBatch(flavors);
        }


        //3. 删除redis缓存中的菜品数据
        cleanCache("*");
    }



    /**
     * 起售/停售菜品
     * @param id
     * @param status
     */
    @Transactional
    @Override
    public void startOrStop(Long id, Integer status) {
        //1. 更新菜品状态信息
        Dish dish = Dish.builder()
                .id(id)
                .status(status)
                .build();
        dishMapper.update(dish);

        //2. 如果是停售操作 , 还需要将该菜品关联的套餐也停售了
        if(status == StatusConstant.DISABLE){
            List<Long> setmealIds = setmealDishMapper.getSetmealIdsByDishIds(Collections.singletonList(id));
            if(!CollectionUtils.isEmpty(setmealIds)){
                setmealIds.stream().forEach(setmealId -> {
                    Setmeal setmeal = Setmeal.builder().id(setmealId).status(StatusConstant.DISABLE).build();
                    setmealMapper.update(setmeal);//停售对应的套餐
                });
            }
        }


        //3. 删除redis缓存中的菜品数据
        cleanCache("*");
    }


    @Override
    public List<Dish> list(Long categoryId, String name) {
        return dishMapper.selectDishByCondition(categoryId, name);
    }


    @Override
    public List<DishVO> listDishWithFlavors(Long categoryId) {
        String redisDishKey = "dish:cache:"+categoryId;

        //1. 先查询redis缓存 , 如果缓存中有数据 , 直接返回
        List<DishVO> dishVOList = (List<DishVO>) redisTemplate.opsForValue().get(redisDishKey);
        if (!CollectionUtils.isEmpty(dishVOList)){
            log.info("查询redis缓存, 命中数据, 直接返回 ....");
            return dishVOList;
        }

        //2. 如果缓存中没有数据, 再查询数据库
        Dish dish = Dish.builder().categoryId(categoryId).status(StatusConstant.ENABLE).build();
        dishVOList = dishMapper.listDishWithFlavors(dish);

        //3. 把数据库查询的结果, 加入缓存
        redisTemplate.opsForValue().set(redisDishKey, dishVOList);
        log.info("查询数据库, 将查询到的数据, 缓存在redis中 ....");
        return dishVOList;
    }
}
